extends Node2D
class_name Level

onready var player_scene = preload("res://object/player.tscn")
onready var raccoon_scene = preload("res://object/raccoon.tscn")
onready var nest = $Nest
onready var spawn_timer: Timer = $SpawnTimer
onready var gameui: Node = $GameUI

export var min_spawn_time: float = 5.0
export var max_spawn_time: float = 10.0
export var max_enemies: int = 2
export var difficulty_ramp_time: float = 60.0

var player: Player = null
var spawners = []
var enemies = []

var play_time: float = 0

func spawn_player():
	assert(player == null)
	assert(nest)
	player = player_scene.instance()
	player.position = nest.position
	add_child(player)

func spawn_raccoon():
	var spawner = spawners[randi() % spawners.size()]
	var raccoon = raccoon_scene.instance()
	raccoon.position = spawner.position
	enemies.append(raccoon)
	add_child(raccoon)
	
func despawn_enemy(enemy: Node2D):
	var idx: int = enemies.find(enemy)
	assert(idx != -1)
	enemies.remove(idx)
	enemy.queue_free()

func egg_destroyed():
	GameGlobal.gameover()

func egg_hatched():
	GameGlobal.level_complete()

func get_nest():
	return nest

func get_ui() -> Node:
	return gameui

func _physics_process(dt: float):
	play_time += dt
	var difficulty: float = pow(play_time, 2) / pow(difficulty_ramp_time, 2)
	difficulty = clamp(difficulty, 0, 1)
	var spawn_time: float = lerp(max_spawn_time, min_spawn_time, difficulty)
	
	if spawn_timer.is_stopped():
		if enemies.size() < max_enemies:
			spawn_raccoon()
		var t: float = rand_range(min_spawn_time, spawn_time)
		spawn_timer.start(t)

func _ready():
	GameGlobal.set_current_level(self)
	assert(nest)
	spawn_player()
	spawners = $Spawners.get_children()

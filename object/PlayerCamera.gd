extends Camera2D

func limit_from_tilemap(tilemap: TileMap):
	var bounds: Rect2 = tilemap.get_used_rect()
	bounds.position = tilemap.map_to_world(bounds.position, true)
	bounds.size *= tilemap.cell_size
	limit_left = int(ceil(bounds.position.x))
	limit_top = int(ceil(bounds.position.y))
	limit_right = int(floor(bounds.position.x + bounds.size.x))
	limit_bottom = int(floor(bounds.position.y + bounds.size.y))

func _ready():
	var level_bg = $"/root/Level/Background"
	if level_bg:
		limit_from_tilemap(level_bg)

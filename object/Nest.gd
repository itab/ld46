extends Area2D
class_name Nest

const NEST_TOLERANCE: float = 3.0
const MAX_HEALTH: int = 5

onready var nest_sprite: AnimatedSprite = $Nest
onready var hightlight_sprite: AnimatedSprite = $Highlight
onready var emitter: CPUParticles2D = $EggshellEmitter
onready var emit_timer: Timer = $EmitTimer

onready var level: Level = $"/root/Level"

export var time_to_hatch = 10.0

var player: Player = null
var game_ui: Node = null
var health: int = MAX_HEALTH
var hatch_accum: float = 0

func set_highlight(highlight: bool):
	hightlight_sprite.visible = highlight

func set_crack_level(level: int):
	nest_sprite.animation = "crack"
	nest_sprite.playing = false
	nest_sprite.frame = level

func emit_shells():
	emitter.emitting = true
	emit_timer.start(0.1)

func damage(damage: int, dir: Vector2):
	if health > 0:
		if player_is_nesting():
			dir = dir + Vector2(0, -1)
			dir = dir.normalized()
			player.kick_out_of_nest(dir)
		else:
			health = clamp(health - damage, 0, MAX_HEALTH)
			set_crack_level(MAX_HEALTH - health)
			emit_shells()

func player_is_sitting() -> bool:
	if player:
		return player.get_current_state() == player.State.SITTING
	return false

func player_is_nesting() -> bool:
	if player:
		return player.get_current_state() == player.State.NESTING
	return false

func _on_body_entered(body: Node):
	if body is Player:
		player = body as Player

func _on_body_exited(body: Node):
	if body == player:
		set_highlight(false)
		player.set_can_nest(false)
		player = null

func _physics_process(dt: float):
	if health < 1:
		level.egg_destroyed()
	if player:
		var dist: float = abs(player.position.x - position.x)
		var can_nest: bool = dist < NEST_TOLERANCE
		player.set_can_nest(can_nest)
		set_highlight(can_nest and not player_is_nesting() and not player_is_sitting())
		if player_is_nesting():
			game_ui.get_node("Tutorial").text = "Peck away predators\nwith [SPACE]"
			hatch_accum += dt
			game_ui.set_meter_progress(hatch_accum / time_to_hatch)
			if hatch_accum >= time_to_hatch:
				level.egg_hatched()

func _ready():
	set_highlight(false)
	game_ui = level.get_node("GameUI")
	assert(game_ui)

func _on_emit_timeout():
	emitter.emitting = false

extends KinematicBody2D
class_name Raccoon

onready var global = $"/root/GameGlobal"
onready var level = $"/root/Level"
onready var sprite: AnimatedSprite = $Sprite
onready var eat_timer: Timer = $EatTimer
onready var audio_player: AudioStreamPlayer2D = $AudioPlayer
onready var startpos: Vector2 = position
var nest = null

const EAT_SOUND_NAMES = ["eat1.wav", "eat2.wav", "eat3.wav", "eat4.wav", "eat5.wav"]
var eat_sounds = []

const GRAVITY: Vector2 = Vector2(0, 100)
const SPEED: float = 40.0
const ENTER_SPEED: float = SPEED
const FLEE_SPEED: float = SPEED * 4.0
const EAT_INTERVAL: float = 1.0
const MAX_HEALTH: int = 1
var health = MAX_HEALTH

enum State {
	ENTERING,
	WALKING,
	EATING,
	FLEEING,
}
var current_state = State.ENTERING
var queued_state = null

var reached_nest = false

func damage(damage: int):
	health = clamp(health - damage, 0, MAX_HEALTH)

#
# TRANSFORMATION
#

func set_flipped(flipped: bool):
	transform.x.x = -1 if flipped else 1

func look_left():
	set_flipped(true)
	
func look_right():
	set_flipped(false)

#
# STATE BEHAVIOR
#

func _exit_state(state):
	match state:
		# TODO: Behaviors
		_: pass

func _enter_state(state):
	match state:
		State.EATING:
			sprite.animation = "default"
			eat_timer.stop()
		
		State.FLEEING:
			sprite.animation = "walk"
			sprite.speed_scale = 2.0

func _process_state(dt: float):
	match current_state:
		State.ENTERING:
			if health == 0:
				switch_state(State.FLEEING)
				return
			
			var direction: Vector2 = nest.position - position
			direction = direction.normalized()
			direction.y = 0
			
			if direction.x > 0:
				look_right()
			elif direction.x < 0:
				look_left()
				
			position += direction * ENTER_SPEED * dt
			
			var touching_wall: bool = true
			
			var collision: KinematicCollision2D = move_and_collide(direction, true, true, true)
			var staticbody: StaticBody2D = null
			if collision:
				staticbody = collision.collider as StaticBody2D
			if not staticbody or not (staticbody.layers & global.PhysicsLayer.Solid):
				touching_wall = false;

			if not touching_wall:
				switch_state(State.WALKING)

		State.WALKING:
			if health == 0:
				switch_state(State.FLEEING)
				return
			
			if reached_nest:
				switch_state(State.EATING)
			
			var direction: Vector2 = nest.position - position
			direction = direction.normalized()
			direction.y = 0
			
			if direction.x > 0:
				look_right()
			elif direction.x < 0:
				look_left()
			
			var velocity: Vector2 = direction * SPEED
			velocity += GRAVITY
			
			move_and_slide(velocity, Vector2.UP)
		
		State.EATING:
			if health == 0:
				switch_state(State.FLEEING)
				return
			
			if eat_timer.is_stopped():
				var direction: Vector2 = nest.position - position
				nest.damage(1, direction.normalized())
				eat_timer.start(EAT_INTERVAL)
				audio_player.stream = eat_sounds[randi() % eat_sounds.size()]
				audio_player.play()
			
		State.FLEEING:
			var direction: Vector2 = startpos - position
			direction = direction.normalized()
			direction.y = 0
			
			if direction.x > 0:
				look_right()
			elif direction.x < 0:
				look_left()
			
			position += direction * FLEE_SPEED * dt
			
			if position.distance_to(startpos) < 2:
				level.despawn_enemy(self)

#
# STATE MANAGEMENT
#

func switch_state(state):
	queued_state = state

func _switch_state(state):
	_exit_state(current_state)
	_enter_state(state)
	current_state = state

func _physics_process(dt: float):
	_process_state(dt)
	if queued_state != null:
		_switch_state(queued_state)
		queued_state = null

func _ready():
	assert(level)
	nest = level.get_nest()
	assert(nest)
	for s in EAT_SOUND_NAMES:
		var sound = load("res://sound/" + s)
		assert(sound)
		eat_sounds.append(sound)

func _on_area_entered(area: Area2D):
	if area == nest:
		reached_nest = true

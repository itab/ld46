extends KinematicBody2D
class_name Player

onready var sprite: AnimatedSprite = $Sprite
onready var peck_timer: Timer = $PeckTimer
onready var jump_timer: Timer = $JumpTimer
onready var stun_timer: Timer = $StunTimer
onready var audio_player: AudioStreamPlayer2D = $AudioPlayer

const PECK_SOUND_FILES = ["peck1.wav", "peck2.wav", "peck3.wav"]
const STUN_SOUND_FILES = ["stun1.wav", "stun2.wav"]
const WALK_SOUND_FILES = ["walk1.wav", "walk2.wav", "walk3.wav"]

var peck_sounds = []
var stun_sounds = []
var walk_sounds = []

const GRAVITY_ACCEL: Vector2 = Vector2(0, 400)
const JUMP_IMPULSE: Vector2 = Vector2(0, -170)
const STUN_IMPULSE: float = 130.0
const MOVE_DOWN_NUDGE: Vector2 = Vector2(0, 1)
const WALK_SPEED: float = 40.0
const PECK_COOLDOWN: float = 0.5
const JUMP_GRACE: float = 0.1
const INITIAL_INPUT_STATE = {
	"left": false,
	"right": false,
	"down": false,
	"jump": false,
	"peck": false,
}

var velocity: Vector2 = Vector2()

var _new_input_state = INITIAL_INPUT_STATE
var input_just_released = INITIAL_INPUT_STATE
var input_just_pressed = INITIAL_INPUT_STATE
var prev_input = INITIAL_INPUT_STATE
var input = INITIAL_INPUT_STATE

enum State {
	FALLING,
	WALKING,
	SITTING,
	STANDING,
	NESTING,
	JUMPING,
	PECKING,
	STUNNED,
}

var current_state = State.FALLING
var queued_state = null

var animation_finished = false

var can_nest: bool = false

var peckable = []

#
# PECKING
#

func is_enemy_type(node: Node):
	return node is Raccoon

func _on_pecking_body_entered(body: Node):
	if is_enemy_type(body):
		peckable.append(body)
		if current_state == State.PECKING:
			body.damage(1)

func _on_pecking_body_exited(body: Node):
	var idx: int = peckable.find(body)
	if idx != -1:
		peckable.remove(idx)
#
# NEST INTERACTION
#

func set_can_nest(val: bool):
	can_nest = val

func kick_out_of_nest(dir: Vector2):
	velocity = dir.normalized() * STUN_IMPULSE
	switch_state(State.STUNNED)

#
# TRANSFORM
#

func set_flipped(flipped: bool):
	transform.x.x = -1 if flipped else 1

func look_left():
	set_flipped(true)
	
func look_right():
	set_flipped(false)

#
# ANIMATION MANAGEMENT
#

func _on_animation_finished():
	animation_finished = true

func play_animation(name: String, speed: float = 1, reset: bool = false):
	animation_finished = false
	if reset:
		sprite.frame = 0
	sprite.speed_scale = speed
	sprite.animation = name

#
# STATE BEHAVIOR
#

func get_current_state() -> int:
	return current_state

func switch_state(state):
	queued_state = state

func _enter_state(state):
	match state:
		State.FALLING:
			# TODO: Animation for falling
			play_animation("default")
		
		State.JUMPING:
			velocity = JUMP_IMPULSE
		
		State.PECKING:
			velocity.x = 0
			play_animation("peck")
			for enemy in peckable:
				enemy.damage(1)
			audio_player.stream = peck_sounds[randi() % peck_sounds.size()]
			audio_player.play()
		
		State.SITTING:
			velocity.x = 0
			play_animation("sit")
		
		State.STANDING:
			velocity.x = 0
			play_animation("stand")
			
		State.STUNNED:
			stun_timer.start(2.0)
			play_animation("stunned")
			audio_player.stream = stun_sounds[randi() % stun_sounds.size()]
			audio_player.play()

func _exit_state(_state):
	pass

func _process_state(dt: float):
	match current_state:
		State.WALKING:
			if not jump_timer.is_stopped() or input_just_pressed["jump"]:
				switch_state(State.JUMPING)
			elif input_just_pressed["peck"]:
				switch_state(State.PECKING)
			elif can_nest and input_just_pressed["down"]:
				switch_state(State.SITTING)
			elif input_just_pressed["down"]:
				position += MOVE_DOWN_NUDGE

			velocity += GRAVITY_ACCEL * dt
			
			velocity.x = 0
			velocity.x += float(input["right"]) * WALK_SPEED
			velocity.x -= float(input["left"]) * WALK_SPEED
			
			if velocity.x < 0:
				look_left()
			if velocity.x > 0:
				look_right()
			
			if abs(velocity.x) > 0:
				play_animation("walk", abs(velocity.x) / WALK_SPEED)
				if sprite.frame == 0:
					audio_player.stream = walk_sounds[randi() % walk_sounds.size()]
					audio_player.play()
			else:
				play_animation("default")

		State.FALLING:
			if input_just_pressed["jump"]:
				jump_timer.one_shot = true
				jump_timer.start(JUMP_GRACE)
			if is_on_floor():
				switch_state(State.WALKING)
			
			velocity += GRAVITY_ACCEL * dt
			
			velocity.x = 0
			velocity.x += float(input["right"]) * WALK_SPEED
			velocity.x -= float(input["left"]) * WALK_SPEED
			
			if velocity.x < 0:
				look_left()
			if velocity.x > 0:
				look_right()

		State.JUMPING:
			switch_state(State.FALLING)

		State.PECKING:
			if animation_finished:
				switch_state(State.WALKING)

		State.SITTING:
			if animation_finished:
				switch_state(State.NESTING)
		
		State.NESTING:
			if input_just_pressed["jump"]:
				switch_state(State.STANDING)
		
		State.STANDING:
			if animation_finished:
				switch_state(State.WALKING)
				
		State.STUNNED:
			if stun_timer.is_stopped():
				switch_state(State.FALLING)
			
			if is_on_floor():
				velocity.x = 0
			
			velocity += GRAVITY_ACCEL * dt
			
			# Reversed on purpose to make the stun animation look a bit better
			if velocity.x < 0:
				look_right()
			if velocity.x > 0:
				look_left()

func _switch_state(state):
	_exit_state(current_state)
	_enter_state(state)
	current_state = state

func _physics_process(dt: float):
	_update_input_state()
	_process_state(dt)
	if queued_state != null:
		_switch_state(queued_state)
		queued_state = null
	velocity = move_and_slide(velocity, Vector2.UP)

#
# INPUT
#

func _update_input_state():
	prev_input = input
	input = _new_input_state.duplicate()
	input_just_pressed = INITIAL_INPUT_STATE.duplicate()
	input_just_released = INITIAL_INPUT_STATE.duplicate()
	for k in input.keys():
		input_just_pressed[k] = input[k] and not prev_input[k]
		input_just_released[k] = not input[k] and prev_input[k]

func input_jump():
	jump_timer.one_shot = true
	jump_timer.start(JUMP_GRACE)
	_new_input_state["jump"] = true

const ANALOG_STICK_THRESHOLD = 0.7

func _unhandled_input(event: InputEvent):
	# Digital Inputs
	if event.is_action_pressed("move_left"):
		_new_input_state["left"] = true
	if event.is_action_released("move_left"):
		_new_input_state["left"] = false
	
	if event.is_action_pressed("move_right"):
		_new_input_state["right"] = true
	if event.is_action_released("move_right"):
		_new_input_state["right"] = false

	if event.is_action_pressed("peck"):
		_new_input_state["peck"] = true
	if event.is_action_released("peck"):
		_new_input_state["peck"] = false

	if event.is_action_pressed("jump"):
		_new_input_state["jump"] = true
	if event.is_action_released("jump"):
		_new_input_state["jump"] = false

	if event.is_action_pressed("move_down"):
		_new_input_state["down"] = true
	if event.is_action_released("move_down"):
		_new_input_state["down"] = false

	# Adapt analog inputs into digital ones
	if event.get_action_strength("jump") > 0:
		_new_input_state["jump"] = event.get_action_strength("jump") > ANALOG_STICK_THRESHOLD
	if event.get_action_strength("move_left") > 0:
		_new_input_state["left"] = event.get_action_strength("move_left") > ANALOG_STICK_THRESHOLD
	if event.get_action_strength("move_right") > 0:
		_new_input_state["right"] = event.get_action_strength("move_right") > ANALOG_STICK_THRESHOLD
	if event.get_action_strength("move_down") > 0:
		_new_input_state["down"] = event.get_action_strength("move_down") > ANALOG_STICK_THRESHOLD

func _ready():
	for s in PECK_SOUND_FILES:
		var soundfile = load("res://sound/" + s)
		assert(soundfile)
		peck_sounds.append(soundfile)
	for s in STUN_SOUND_FILES:
		var soundfile = load("res://sound/" + s)
		assert(soundfile)
		stun_sounds.append(soundfile)
	for s in WALK_SOUND_FILES:
		var soundfile = load("res://sound/" + s)
		assert(soundfile)
		walk_sounds.append(soundfile)

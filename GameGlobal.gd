extends Node

enum PhysicsLayer {
	Solid = 1 << 0,
	OneWay = 1 << 1,
	Characters = 1 << 2,
	Items = 1 << 3,
}

onready var pause_menu_scene = preload("res://ui/pausemenu.tscn")

var current_level_id: int = 0
var current_level: Node2D = null
var pause_menu = null
var gameover_menu = null

func unload_level():
	if pause_menu:
		pause_menu.queue_free()
		pause_menu = null
	if gameover_menu:
		gameover_menu.queue_free()
		gameover_menu = null
	if current_level:
		current_level.queue_free()
		current_level = null

func load_level(id: int):
	unload_level()
	current_level_id = id
	var path = "res://level/level_%d.tscn" % id
	var res = get_tree().change_scene(path)
	assert(res == OK)

func load_first_level():
	return load_level(1)

func restart_level():
	load_level(current_level_id)
	get_tree().paused = false

func quit_level():
	unload_level()
	var res = get_tree().change_scene("res://ui/mainmenu.tscn")
	assert(res == OK)

func set_current_level(level):
	current_level = level

func is_paused() -> bool:
	return pause_menu != null

func is_gameover() -> bool:
	return gameover_menu != null

func pause_unpause():
	if is_gameover():
		return
	if pause_menu:
		pause_menu.queue_free()
		pause_menu = null
		get_tree().paused = false
	else:
		if current_level:
			pause_menu = pause_menu_scene.instance()
			assert(pause_menu)
			current_level.add_child(pause_menu)
			pause_menu.pause_mode = Node.PAUSE_MODE_PROCESS
			pause_menu.set_can_resume(true)
			pause_menu.set_text("Paused")
			get_tree().paused = true

func gameover():
	if not current_level:
		return
	if is_paused():
		pause_unpause()
	gameover_menu = pause_menu_scene.instance()
	assert(gameover_menu)
	current_level.add_child(gameover_menu)
	gameover_menu.pause_mode = Node.PAUSE_MODE_PROCESS
	gameover_menu.set_can_resume(false)
	gameover_menu.set_text("Game Over")
	get_tree().paused = true

func level_complete():
	if not current_level:
		return
	if is_paused():
		pause_unpause()
	gameover_menu = pause_menu_scene.instance()
	assert(gameover_menu)
	current_level.add_child(gameover_menu)
	gameover_menu.pause_mode = Node.PAUSE_MODE_PROCESS
	gameover_menu.set_can_resume(false)
	gameover_menu.set_text("Egg Hatched!")
	get_tree().paused = true

func _ready():
	pause_mode = Node.PAUSE_MODE_PROCESS

func _unhandled_input(event: InputEvent):
	if event.is_action_pressed("pause"):
		pause_unpause()

func _unhandled_key_input(event: InputEventKey):
	# DEBUG KEYS
	if OS.is_debug_build():
		if event.pressed:
			if event.scancode == KEY_Q:
				get_tree().quit(0)
			if event.scancode == KEY_R:
				load_level(1)

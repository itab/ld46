extends Control

onready var play_button: Button = $Buttons/PlayButton
onready var quit_button: Button = $Buttons/PlayButton

func _ready():
	get_tree().paused = false
	play_button.grab_focus()

func _on_play():
	GameGlobal.load_first_level()

func _on_quit():
	get_tree().quit(0)

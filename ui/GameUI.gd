extends CanvasLayer

onready var arrow: TextureRect = $Meter/Arrow
onready var tutorial: Label = $Tutorial

const TUTORUIAL_SHOW_TIME: float = 20.0
var tutorial_shown: float = 0

func set_meter_progress(progress: float):
	progress = clamp(progress, 0, 1)
	var rotation: float = deg2rad(progress * 90 - 45)
	arrow.set_rotation(rotation)

func _ready():
	set_meter_progress(0)

func _process(dt: float):
	tutorial_shown += dt
	var tutorial_alpha: float = 1 - clamp((tutorial_shown - 30) / 5, 0, 1)
	if tutorial_shown > TUTORUIAL_SHOW_TIME:
		tutorial.modulate.a = tutorial_alpha

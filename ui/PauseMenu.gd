extends CanvasLayer

onready var label: Label = $UI/Label
onready var resume_btn: Button = $UI/Buttons/Resume
onready var restart_btn: Button = $UI/Buttons/Restart
onready var quit_btn: Button = $UI/Buttons/Quit

func set_can_resume(resume: bool):
	resume_btn.visible = resume
	restart_btn.visible = not resume
	if resume:
		resume_btn.grab_focus()
	else:
		restart_btn.grab_focus()
	
func set_text(text: String):
	label.text = text

func _on_resume():
	GameGlobal.pause_unpause()
	
func _on_restart():
	GameGlobal.restart_level()

func _on_quit():
	GameGlobal.quit_level()

func _ready():
	assert(label)
	assert(resume_btn)
	assert(restart_btn)
	assert(quit_btn)
